<?php

namespace App\Models;

use illuminate\Database\Eloquent\Model;

class Serie extends Model
{
    public $timestamps = false;
    protected $fillable = ['temporada', 'numer', 'assistido'];

    public function episodios()
    {
        return $this->belongsTo(Serie::class);
    }
}
